const fs = require('fs');
const { decodeToken } = require('../validation.js');
const mailTemplate = require('./template.js');
const debug = require('../debugLog.js');

module.exports = {
  TEMPLATE: {
    VALIDATION: 'validation-requested'
  },

  send: (transporter, token, props) => {
    if (!token) {
      console.error('ERROR: Missing token');
      return;
    }
    decodeToken(
      reqToken,
      (invoker) => {
        const { email, name } = invoker;
        module.exports.sendTo(transporter, {
          ...props,
          email,
          name,
          invoker
        });
      },
      (e) => console.error(e)
    );
  },

  sendTo: (transporter, props) => {
    const { subject, to } = props;
    return mailTemplate.buildHTML(props, (msgObj) => {
      transporter.sendMail(msgObj, (e) => {
        if (e) {
          console.error(e);
        } else {
          debug.log(`EMAIL: Sent ${subject} To: ${to}`);
        }
      });
    });
  }
};
