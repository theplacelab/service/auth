const { v4: uuidv4 } = require('uuid');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { debug } = require('console');
const generator = require('generate-password');
const saltRounds = 10;
const { ROLE, RESPONSE } = require('../../constants.js');
const {
  malformed,
  created,
  ok,
  conflict,
  notFound,
  unauthorized
} = require('../util.js');

module.exports = {
  create: async (knex, data, cb) => {
    const email = data.email.toLowerCase();
    const [emailInUse] = await knex.select().from('users').where({ email });
    if (emailInUse) {
      delete data.pass;
      return cb(conflict({ data }));
    } else {
      bcrypt.hash(data.pass, saltRounds, async (err, hash) => {
        delete data.pass;
        if (err) throw err;
        try {
          const [{ id }] = await knex('users')
            .insert([
              {
                email,
                password: hash,
                uuid: uuidv4(),
                created_at: new Date().toISOString(),
                role: data.role ? data.role : ROLE.USER,
                ...data
              }
            ])
            .returning(['id']);
          const [newUser] = await knex.select().from('users').where({ id });
          delete newUser.password;
          delete newUser.refresh_secret;
          delete newUser.id;
          return cb(created({ data: newUser }));
        } catch (error) {
          return cb(malformed({ data, error }));
        }
      });
    }
  },
  read: async (knex, data, cb) => {
    try {
      const user = await knex
        .select('*')
        .from('users')
        .modify((q) => {
          if (data.uuid !== '*') q.where('uuid', data.uuid);
        });
      user.forEach((item) => {
        delete item.id;
        delete item.refresh_secret;
        delete item.password;
      });
      if (user.length === 0) return cb(notFound({ data }));
      cb(ok({ data: user.length === 1 ? user[0] : user }));
    } catch (error) {
      cb(malformed({ data, error }));
    }
  },
  update: async (req, cb) => {
    const { knex, body } = req;

    const { uuid } = req.params;
    const { email, name, role, avatar } = body;
    const [targetUser] = await knex.select().from('users');
    if (!targetUser) return cb(notFound({ data }));

    // Only validated users can be updated
    if (targetUser.f_valid === 0 && req.token.role !== ROLE.SUPER)
      return cb(unauthorized({ data: req.body }));

    // Only ROLE.SUPER can change user roles
    if (role && role !== targetUser.role && req.token.role !== ROLE.SUPER)
      return cb(unauthorized({ data: req.body }));

    // Emails must be unique
    if (email) {
      const [emailInUse] = await req.knex
        .select('email', 'uuid')
        .from('users')
        .where({ email })
        .andWhereNot({ uuid });
      if (emailInUse) return cb(conflict({ data: req.body }));
    }

    //try {
    await knex('users').where({ uuid }).update({
      email: email?.toLowerCase(),
      name,
      role,
      avatar
    });
    const [user] = await knex.select().from('users').where({ uuid });
    delete user.password;
    delete user.refresh_secret;
    delete user.id;
    return cb(ok({ data: user.length === 1 ? user[0] : user }));
    /*} catch (error) {
      console.error(error);
      cb(malformed({ data, error }));
    }*/
  },
  destroy: async (knex, data, cb) => {
    data.email = data.email ? data.email.toLowerCase() : data.email;
    await knex('users').where({ uuid: data.uuid }).del();
    cb(ok({ data }));
  },
  login: async (knex, data, cb) => {
    data.email = data.email ? data.email.toLowerCase() : data.email;

    if (!(data?.email && data?.pass)) {
      cb(null, RESPONSE.NOT_AUTHORIZED);
      return;
    }
    const [user] = await knex
      .select('*')
      .from('users')
      .where({ email: data?.email });
    if (user) {
      bcrypt.compare(data.pass, user.password, async (err, correctPassword) => {
        if (err) console.error(err);
        if (correctPassword) {
          const tokens = await module.exports.issueTokens(
            knex,
            user,
            data.extend
          );
          cb(tokens, RESPONSE.OK);
        } else {
          cb(null, RESPONSE.NOT_AUTHORIZED);
        }
      });
    } else {
      cb(null, RESPONSE.NOT_AUTHORIZED);
    }
  },
  status: async (knex, data, cb) => {
    data.email = data.email ? data.email.toLowerCase() : data.email;
    const [user] = await knex
      .select('email', 'f_reset', 'f_valid')
      .from('users')
      .where({ email: data.email });
    if (user) {
      cb(ok({ data: user }));
    } else {
      cb(notFound({ data: user }));
    }
  },
  reset: async (knex, email, cb) => {
    const [user] = await knex.select().from('users').where({ email });
    if (!user) return cb(notFound(email));
    const new_password = generator.generate({
      length: 20,
      numbers: true
    });

    bcrypt.hash(new_password, saltRounds, async (err, hash) => {
      if (err) throw err;
      await knex('users').where({ uuid: user.uuid }).update({
        password: hash,
        f_valid: false,
        f_reset: true
      });
      return cb(ok({ data: { email, new_password } }));
    });
  },
  issueTokens: async (knex, user, extend) => {
    const auth = jwt.sign(
      {
        email: user.email,
        uuid: user.uuid,
        name: user.name,
        role: user.role,
        f_reset: user.f_reset,
        f_valid: user.f_valid,
        exp: Math.floor(Date.now() / 1000) + 1 * 60
      },
      `${process.env.JWT_SECRET}`
    );
    const refresh_secret = generator.generate({
      length: 40,
      numbers: true
    });
    await knex('users').where({ uuid: user.uuid }).update({ refresh_secret });
    const refresh = jwt.sign(
      {
        ...{
          uuid: user.uuid,
          extended: extend
        },
        exp: extend
          ? Math.floor(Date.now() / 1000) + 60 * 24 * 7 * 60 // ~1 week from now
          : Math.floor(Date.now() / 1000) + 30 * 60 // ~30 min from now
      },
      refresh_secret
    );
    return { auth, extend: extend, refresh };
  },
  changePassword: async (knex, data, cb) => {
    const [user] = await knex
      .select('*')
      .from('users')
      .where({ email: data.email });
    if (user) {
      bcrypt.compare(data.pass, user.password, async (err, correctPassword) => {
        if (!correctPassword) return cb(null, RESPONSE.NOT_AUTHORIZED);

        if (data.new_password.trim().length === 0)
          return cb(null, RESPONSE.BAD_REQUEST);
        if (!(data.new_password === data.new_password_confirm)) {
          return cb(null, RESPONSE.BAD_REQUEST);
        }
        if (err) {
          console.error(err);
          return cb(null, RESPONSE.BAD_REQUEST);
        }

        bcrypt.hash(data.new_password, saltRounds, async (err, hash) => {
          if (err) {
            return cb(null, RESPONSE.BAD_REQUEST);
          }
          await knex('users').where({ id: user.id }).update({
            password: hash,
            f_reset: false
          });
          if (!user) {
            return cb(null, RESPONSE.NOT_AUTHORIZED);
          } else {
            return cb(JSON.stringify(user), RESPONSE.OK);
          }
        });
      });
    } else {
      return cb(null, RESPONSE.NOT_AUTHORIZED);
    }
  }
};
