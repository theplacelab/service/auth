const router = require('express').Router();
const { RESPONSE } = require('../constants');

router.get('/up', (req, res) => {
  res.status(RESPONSE.OK).send('AUTH');
});

module.exports = router;
