const router = require('express').Router();
const user = require('../data/models/user');
const { ROLE } = require('../constants');

const { requireMinimumRole } = require('../modules/validation');

router.get('/', (req, res) => {
  requireMinimumRole(ROLE.SUPER, req, res, () => {
    user.read(req.knex, { id: '*' }, (response, code) => {
      res.status(code).send(response);
    });
  });
});

module.exports = router;
