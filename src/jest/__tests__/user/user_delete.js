const request = require('supertest');
const server = require('../../../app');
const { RESPONSE, ROLE } = require('../../../constants.js');
const {
  knex,
  before,
  after,
  validTokenFor,
  generateUser
} = require('../common.js');
const { testUsers } = require('../../../data/seeds/01_test_users');
beforeAll(before);
afterAll(after);

describe(`API: Delete User`, () => {
  test('SUPER: Delete self should delete record', (done) => {
    const superUser = generateUser({ role: ROLE.SUPER });
    server.start(
      () => {
        request(server.app)
          .put(`/user`)
          .send(superUser)
          .set(
            'Authorization',
            `bearer ${validTokenFor(generateUser({ role: ROLE.SUPER }))}`
          )
          .end((err, res) => {
            if (err) throw err;
            const targetUser = JSON.parse(res.text);
            const validToken = validTokenFor(targetUser);
            request(server.app)
              .delete(`/user/${targetUser.uuid}`)
              .set('Authorization', `bearer ${validToken}`)
              .end((err, response) => {
                if (err) throw err;
                expect(response.statusCode).toEqual(RESPONSE.OK);
                request(server.app)
                  .get(`/user/${targetUser.uuid}`)
                  .set('Authorization', `bearer ${validToken}`)
                  .end((err, response) => {
                    if (err) throw err;
                    expect(response.statusCode).toEqual(RESPONSE.NOT_FOUND);
                    done();
                  });
              });
          });
      },
      knex,
      true
    );
  });

  test('USER: Delete self should delete record', (done) => {
    server.start(
      () => {
        request(server.app)
          .put(`/user`)
          .send(generateUser())
          .end((err, res) => {
            if (err) throw err;
            const user = JSON.parse(res.text);
            request(server.app)
              .delete(`/user/${user.uuid}`)
              .set('Authorization', `bearer ${validTokenFor(user)}`)
              .end((err, response) => {
                if (err) throw err;
                expect(response.statusCode).toEqual(RESPONSE.OK);
                request(server.app)
                  .get(`/user/${user.uuid}`)
                  .set(
                    'Authorization',
                    `bearer ${validTokenFor(
                      testUsers.find((user) => user.role === ROLE.SUPER)
                    )}`
                  )
                  .end((err, response) => {
                    if (err) throw err;
                    expect(response.statusCode).toEqual(RESPONSE.NOT_FOUND);
                    done();
                  });
              });
          });
      },
      knex,
      true
    );
  });

  test('SUPER: Delete other user by uuid should delete record', (done) => {
    const requestingUser = generateUser({ role: ROLE.SUPER });
    const targetUser = generateUser();
    server.start(
      () => {
        request(server.app)
          .put(`/user`)
          .send(targetUser)
          .end((err, res) => {
            if (err) throw err;
            const { uuid } = JSON.parse(res.text);
            request(server.app)
              .delete(`/user/${uuid}`)
              .set('Authorization', `bearer ${validTokenFor(requestingUser)}`)
              .end((err, response) => {
                if (err) throw err;
                expect(response.statusCode).toEqual(RESPONSE.OK);
                request(server.app)
                  .get(`/user/${uuid}`)
                  .set(
                    'Authorization',
                    `bearer ${validTokenFor(requestingUser)}`
                  )
                  .end((err, response) => {
                    if (err) throw err;
                    expect(response.statusCode).toEqual(RESPONSE.NOT_FOUND);
                    done();
                  });
              });
          });
      },
      knex,
      true
    );
  });

  test('USER: Delete other user by uuid should fail', (done) => {
    const requestingUser = generateUser();
    server.start(
      () => {
        request(server.app)
          .delete(`/user/${testUsers[0].uuid}`)
          .set('Authorization', `bearer ${validTokenFor(requestingUser)}`)
          .end((err, response) => {
            if (err) throw err;
            expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            done();
          });
      },
      knex,
      true
    );
  });

  test('ANON: Delete user by uuid should fail', (done) => {
    const requestingUser = generateUser();
    server.start(
      () => {
        request(server.app)
          .delete(`/user/${testUsers[1].uuid}`)
          .end((err, response) => {
            if (err) throw err;
            expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            done();
          });
      },
      knex,
      true
    );
  });
});
