const request = require('supertest');
const server = require('../../../app');
const { knex, before, after } = require('../common.js');
const { testUsers } = require('../../../data/seeds/01_test_users');
const jwt = require('jsonwebtoken');
const { ROLE, RESPONSE } = require('../../../constants.js');

beforeAll(before);
afterAll(after);

describe(`API: Reauth`, () => {
  test(`API: RT can be used to log in`, (done) => {
    const testUser = testUsers.find((user) => user.role === ROLE.USER);
    server.start(
      () => {
        request(server.app)
          .post(`/login`)
          .send({
            email: testUser.email,
            pass: testUser.password
          })
          .then((response) => {
            const rt = JSON.parse(response.text).refresh;
            request(server.app)
              .post(`/reauthorize`)
              .set('Authorization', `bearer ${rt}`)
              .then((response) => {
                const authToken = JSON.parse(response.text).auth;
                jwt.verify(
                  authToken,
                  process.env.JWT_SECRET,
                  { ignoreExpiration: false },
                  (_err, decodedToken) => {
                    expect(decodedToken.name).toEqual(testUser.name);
                  }
                );
                done();
              });
          });
      },
      knex,
      true
    );
  });

  test(`API: RT is single use`, (done) => {
    const testUser = testUsers.find((user) => user.role === ROLE.USER);
    server.start(
      () => {
        request(server.app)
          .post(`/login`)
          .send({
            email: testUser.email,
            pass: testUser.password
          })
          .then((response) => {
            const rt = JSON.parse(response.text).refresh;
            request(server.app)
              .post(`/reauthorize`)
              .set('Authorization', `bearer ${rt}`)
              .then((response) => {
                const authToken = JSON.parse(response.text).auth;
                jwt.verify(
                  authToken,
                  process.env.JWT_SECRET,
                  { ignoreExpiration: false },
                  (err, decodedToken) => {
                    expect(decodedToken.name).toEqual(testUser.name);
                    if (err) {
                      done();
                    } else {
                      request(server.app)
                        .post(`/reauthorize`)
                        .set('Authorization', `bearer ${rt}`)
                        .then((response) => {
                          expect(response.statusCode).toEqual(
                            RESPONSE.NOT_AUTHORIZED
                          );
                          done();
                        });
                    }
                  }
                );
                done();
              });
          });
      },
      knex,
      true
    );
  });

  test(`API: Empty request will fail`, (done) => {
    request(server.app)
      .post(`/reauthorize`)
      .then((response) => {
        expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
        done();
      });
  });

  test(`API: Forged token will fail`, (done) => {
    const forgedToken = jwt.sign(
      {
        id: 2,
        extended: true,
        exp: 1652108223,
        iat: 1651503423
      },
      'BAD_PASSWORD'
    );
    request(server.app)
      .post(`/reauthorize`)
      .set('Authorization', `bearer ${forgedToken}`)
      .then((response) => {
        expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
        done();
      });
  });

  test(`API: Expired token will fail`, (done) => {
    const refresh_secret = 'BAD_PASSWORD';
    knex('users')
      .where({ id: 1 })
      .update({ refresh_secret })
      .then(() => {
        const rt = jwt.sign(
          {
            id: 1,
            extended: true,
            exp: 1000000000,
            iat: 1651503423
          },
          refresh_secret
        );
        request(server.app)
          .post(`/reauthorize`)
          .set('Authorization', `bearer ${rt}`)
          .then((response) => {
            expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            done();
          });
      });
  });
});
