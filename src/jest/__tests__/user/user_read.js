const request = require('supertest');
const server = require('../../../app');
const { knex, before, after, validTokenFor } = require('../common.js');
const { testUsers } = require('../../../data/seeds/01_test_users');
const { ROLE, RESPONSE } = require('../../../constants.js');

beforeAll(before);
afterAll(after);

describe(`API: Get User(s)`, () => {
  test('ANON: Get users should fail', (done) => {
    server.start(
      () => {
        request(server.app)
          .get(`/users`)
          .end((err, res) => {
            if (err) throw err;
            expect(res.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            done();
          });
      },
      knex,
      true
    );
  });

  test('USER: Get users should fail', (done) => {
    server.start(() => {
      request(server.app)
        .get(`/users`)
        .set(
          'Authorization',
          `bearer ${validTokenFor(
            testUsers.find((user) => user.role === ROLE.USER)
          )}`
        )
        .end((err, res) => {
          if (err) throw err;
          expect(res.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  test('SUPER: Get users should return array of users', (done) => {
    server.start(
      () => {
        request(server.app)
          .get(`/users`)
          .set(
            'Authorization',
            `bearer ${validTokenFor(
              testUsers.find((user) => user.role === ROLE.SUPER)
            )}`
          )
          .end((err, res) => {
            if (err) throw err;
            expect(res.statusCode).toEqual(RESPONSE.OK);
            expect(Array.isArray(JSON.parse(res.text)));
            done();
          });
      },
      knex,
      true
    );
  });
});

describe(`API: Get User`, () => {
  test('ANON: Get user by uuid should fail', (done) => {
    server.start(() => {
      request(server.app)
        .get(`/users/${testUsers[0]}.uuid`)
        .end((err, res) => {
          if (err) throw err;
          expect(res.statusCode).toEqual(RESPONSE.NOT_FOUND);
          done();
        });
    }, knex);
  });

  test('USER: Get self by uuid should succeed', (done) => {
    const testUser = testUsers.find((user) => user.role === ROLE.USER);
    server.start(
      () => {
        request(server.app)
          .get(`/user/${testUser.uuid}`)
          .set('Authorization', `bearer ${validTokenFor(testUser)}`)
          .end((err, res) => {
            expect(res.statusCode).toEqual(RESPONSE.OK);
            expect(JSON.parse(res.text).name).toEqual(testUser.name);
            if (err) throw err;
            done();
          });
      },
      knex,
      true
    );
  });

  test('USER: Get another user by uuid should fail', (done) => {
    const requestingUser = testUsers.find((user) => user.role === ROLE.USER);
    const targetUser = testUsers.find(
      (user) => user.role === ROLE.USER && user.uuid !== requestingUser.id
    );
    server.start(
      () => {
        request(server.app)
          .get(`/user/${targetUser.id}`)
          .set('Authorization', `bearer ${validTokenFor(requestingUser)}`)
          .end((err, res) => {
            expect(res.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            if (err) throw err;
            done();
          });
      },
      knex,
      true
    );
  });

  test('SUPER: Get user by uuid should return user', (done) => {
    const requestingUser = testUsers.find((user) => user.role === ROLE.SUPER);
    const targetUser = testUsers.find((user) => user.role === ROLE.USER);
    server.start(
      () => {
        request(server.app)
          .get(`/user/${targetUser.uuid}`)
          .set('Authorization', `bearer ${validTokenFor(requestingUser)}`)
          .end((err, res) => {
            expect(res.statusCode).toEqual(RESPONSE.OK);
            const returnedUser = JSON.parse(res.text);
            expect(returnedUser.name).toEqual(targetUser.name);
            expect(Array.isArray(JSON.parse(res.text)));
            if (err) throw err;
            done();
          });
      },
      knex,
      true
    );
  });
});
