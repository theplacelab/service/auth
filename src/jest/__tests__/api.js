const request = require('supertest');
const server = require('../../app');
const { v4: uuidv4 } = require('uuid');
const { RESPONSE } = require('../../constants.js');

const {
  ROLE,
  knex,
  before,
  after,
  validTokenFor,
  testUsers
} = require('./common.js');

beforeAll(before);
afterAll(after);

describe(`API`, () => {
  test('Up should respond with 200', (done) => {
    server.start(() => {
      request(server.app)
        .get('/up')
        .then((response) => {
          try {
            expect(response.statusCode).toBe(RESPONSE.OK);
            done();
          } catch (e) {
            done(e);
          }
        });
    }, knex);
  });

  test('Bogus endpoint should respond with 404', (done) => {
    server.start(() => {
      request(server.app)
        .get(`/${uuidv4()}`)
        .then((response) => {
          try {
            expect(response.statusCode).toBe(RESPONSE.NOT_FOUND);
            done();
          } catch (e) {
            done(e);
          }
        });
    }, knex);
  });
});
