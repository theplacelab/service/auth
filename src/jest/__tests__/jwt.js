let jwt = require('jsonwebtoken');
let jwt_module = require('../../modules/validation.js');
const { RESPONSE, ROLE } = require('../../constants.js');

const jwtSecret = 'testsecret';
process.env.JWT_SECRET = jwtSecret;
const token = jwt.sign(
  {
    role: ROLE.USER,
    exp: Math.floor(Date.now() / 1000) + 1 * 60
  },
  jwtSecret
);
const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.sendStatus = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};
const res = mockResponse();

describe(`Unit: JWT Validation`, () => {
  const mockRequest = (token) => {
    return {
      headers: { authorization: `bearer ${token}` }
    };
  };

  test('Valid token should validate', () => {
    const req = mockRequest(token);
    const onValid = jest.fn();
    jwt_module.requireValidToken(req, res, onValid);
    expect(onValid).toHaveBeenCalled();
  });

  test('Invalid token should fallback', () => {
    const req = mockRequest('NOT-A-TOKEN');
    const onValid = jest.fn();
    const onFallback = jest.fn();
    jwt_module.requireValidToken(req, res, onValid, onFallback);
    expect(onFallback).toHaveBeenCalled();
  });

  test('Decoded token should be injected into req object', () => {
    const req = mockRequest(token);
    const onValid = jest.fn();
    jwt_module.requireValidToken(req, res, onValid);
    expect(req.token.role).toBe(ROLE.USER);
  });

  test('Missing token should 401', () => {
    const req = mockRequest();
    const onValid = jest.fn();
    jwt_module.requireValidToken(req, res, onValid);
    expect(res.sendStatus).toHaveBeenCalledWith(RESPONSE.NOT_AUTHORIZED);
  });

  test('Corrupt token should 401', () => {
    const req = mockRequest(`${token}${Math.random()}`);
    const onValid = jest.fn();
    jwt_module.requireValidToken(req, res, onValid);
    expect(res.sendStatus).toHaveBeenCalledWith(RESPONSE.NOT_AUTHORIZED);
  });
});
