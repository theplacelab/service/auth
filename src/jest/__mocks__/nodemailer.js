let _sentMail = [];

const NodemailerMock = () => {
  const mock = {
    getSentMail: () => _sentMail,
    addMail: (mail) => {
      //console.debug(`Adding ${_sentMail.length}: ${mail.to}`);
      _sentMail.push(mail);
    }
  };
  const createTransport = () => {
    return {
      sendMail(mail, cb) {
        mock.addMail(mail);
        if (typeof cb === 'function') cb();
        if (typeof mock.onSend === 'function') mock.onSend();
      }
    };
  };
  return {
    createTransport,
    mock
  };
};

module.exports = NodemailerMock();
