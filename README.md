# Maptool Auth Service

Source code for the node-based API that supports JWT-based user authorization, login/logout and account creation with email confirmation and self-service update.

Most likely you just want to make use of the docker image and won't need to mess with this code. Check the deployment directory for K8 sample configs.

## /src

Contains the source code for the node server.  
Check `/src/package.json` for useful development scripts.

## /config

[deprecated?]
Sample K8 and docker files.
